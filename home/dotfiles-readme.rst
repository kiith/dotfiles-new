------------
Dependencies
------------

Required:
* homeshick (for dotfiles management)
* nvim
* fzf
* zsh (just use default config, will be overwritten)
* oh-my-zsh
* sudo pip3 install --upgrade neovim
* libnotify-bind (for notify-send)
* fava
* syncthing

Optional:
* exa
* kitty
* haxor-news
* rtv
* tig
* streamlink
* glances

  - pymdstat
  - wifi
  - py-cpuinfo
  - netifaces

Almost useless:
* neofetch

--------------
Stuff to learn
--------------

Tools:

* glances            // instead of htop
* tldr               // instead of/together with man
* transfer FILENAME  // upload any file to transfer.sh - for 14 days
* tig                // for staging - about as good as magit!
* streamlink         // for youtube/twitch/etc downloading - streamlink LINK best -o OUTPUT
* icat               // image cat (kitty builtin)
* haxor-news
* rtv

Vim:

* <PageUp> instead of <C-w> for window commands
* <C-^> to toggle previous file
* yss<CHAR> to surround line with CHAR
* ]c for next git hunk, ]C across buffers
* [c for prev git hunk, [C across buffers
* <Leader>hs to stage current git hunk

Zsh:

* <C-t> FZF filename insertion
* kill -9 <Tab> FZF process search
* cd /usr/** <Tab> FZF path completion
* grep -r 'stuff' | fzf FZF filtering
* <A-c> FZF cd
* <C-r> FZF history search

Tridactyl:
* b: change tab (thru filtering)
* H: back
* L: forward
* t: new tab
* o: open in current tab
* d: close tab
* r: reload
* s: search history etc.
* yy: copy current URL
* p/P: open clipboard URL as web page / in new tab
* zi/zo/zz: zoom in/out/reset
* /: search text, <C-g>/<C-G> to move between matches
* ]]: go to next page
* gF: hints for mass-opening links
* ;y: hints for copying links to clipboard
* ;p: hints for copying paragraphs to clipboard
* ;i/;I: hints for opening images in current/new tab
* ;gi/;gI: hints for mass-opening images in current/new tab
* ;k: hints for killing elements
* ;gk: hints for mass-killing elements
* ;s/;S: hints for save anything/image
* ;a/;A: hints for save-as anything/image
  ;gs/;gS/;ga/;gA: as above but mass-saving
* ;;: hints to focus on element
* :bind <KEYS>: find out what KEYS are bound to
* :viewsource : view page source
* help [command/key] : help for a command/key

Kitty:

* <S-PgUp>     Scroll up
* <S-PgDn>     Scroll down
* <C-S-]>      Next window
* <C-p>        Read scrollback in vim
* <C-S-e>      URL hints
* <C-S-p> l    Line hints
* <C-S-p> f    Filename hints
* <C-S-p> w    Word hints
* <C-S-p> h    Hash hints
* <C-S-n>      New kitty
* <C-S-w>      Close kitty
* <C-S-enter>  New subwindow
* <C-S-Number> Go to window Number
* <C-S-f>      Move window
* <C-S-r>      Resize window
* <C-S-q>      Close tab
* <C-S-f2>     Config kitty
* <C-S-l>      Change layout
