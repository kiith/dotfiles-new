# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:/snap/bin:/opt/wine-devel/bin:$HOME/.cargo/bin:$HOME/go/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  # base commands:
  # .., ..., ...., etc: go up 1, 2, 3, etc. dirs
  # take: cd w/ automatic mkdir
  # d: list recent dirs
  # 1,2,3...9: go to first, second, etc. recent dir as shown by 'dirs'
  # md: mkdir -p
  git                   # ggp: git push origin $(current_branch)
  catimg                # `catimg`: draw image in terminal
  colored-man-pages
  command-not-found
  per-directory-history
  alias-finder          # find existing aliases to commands, e.g. 'alias-finder "git pull"'
  copydir               # `copydir` copies $PWD to clipboard
  copyfile              # `copyfile $FILE` copies contents of $FILE to clipboard
  cp                    # `cpv` - smarter, rsync-based cp
  dircycle              # C-<Left>/C-<Right> to cycle between recent directories
  extract               # `x`/`extract` to extract any archive, making directory if there is none
  history               # `hs mv` will search history for the 'mv' command
  jump                  # `mark NAME` creates a bookmark in current dir, `jump NAME` jumps to it.
  pass                  # completion for `pass` passoword manager
  rsync                 # `rsync-copy`, `rsync-move`, `rsync-update`, `rsync-synchronize`
  rust                  # completion for `rustc`
  rustup                # completion for `rustup`
  safe-paste            # don't automatically execute pasted commands
  sudo                  # double-<Esc> to repeat previous command with `sudo`
  themes                # `lsheme` to list zsh themes, `theme $NAME` to set theme
  timer                 # show time spent by each command when it finishes
  tmux                  # `ta` for tmux attach
  transfer              # `transfer $FILE` will upload a file (or even a directory) to transfer.sh
  zsh-interactive-cd    # `cd <Tab>` will launch fzf autocompletion for `cd`.
)

# homeshick config
source "$HOME/.homesick/repos/homeshick/homeshick.sh"
fpath=($HOME/.homesick/repos/homeshick/completions $fpath)

source $ZSH/oh-my-zsh.sh

# User configuration
HISTFILE=~/.histfile
HISTSIZE=65535
SAVEHIST=65535
setopt appendhistory autocd notify
unsetopt beep

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

export editor='nvim'

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
alias zshcfg="nvim ~/.zshrc"
alias nvimcfg="nvim ~/.config/nvim/init.vim"
# alias ohmyzsh="mate ~/.oh-my-zsh"

alias grep='grep --color'
alias t='tail -f'
alias -g L="| less"
alias ff='find . -type f -name'
alias psmem='ps -e -orss=,args= | sort -b -k1 -nr | head -10' # show memory hogs
alias pscpu='ps -e -o pcpu,cpu,nice,state,cputime,args |sort -k1 -nr | head -10' # show CPU hogs


# End of lines configured by zsh-newuser-install

# exa should only use long grid view if we have >20 files to list
export EXA_GRID_ROWS=20

if type "exa" > /dev/null; then
  alias ls="exa -a"
  alias ll="exa -alBF --git --time-style=long-iso --grid --header --color-scale"
  alias lt="exa --tree --git -alBF --time-style=long-iso --level=2 --header --color-scale"
fi

# enable FZF shortcuts
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

bindkey '^[[1;5D' insert-cycledleft
bindkey '^[[1;5C' insert-cycledright

source "$HOME/bin/transfer.sh"

# Created by `userpath` on 2019-09-16 08:49:50
export PATH="$PATH:/home/kiith-sa/.local/bin"
export RUST_BACKTRACE=1
