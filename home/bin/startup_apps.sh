#!/bin/bash

yakuake &
mkdir -p ~/logs
syncthing -logfile ~/logs/syncthing.log -audit &

konsole -e 'tmux' || mate-terminal -e 'tmux'

fava ~/Sync/finance/data.beancount &
